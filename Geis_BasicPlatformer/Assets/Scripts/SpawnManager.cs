﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public int maxPlatforms = 20;
    public GameObject platform;
    public float horizMin = 6.5f;
    public float horizMax = 14f;
    public float vertMin = -6f;
    public float vertMax = 6f;

    private Vector2 originPosition;

    // Start is called before the first frame update
    void Start()
    {
        originPosition = transform.position;
        Spawn();
    }

    void Spawn()
    {
        for(int i = 0; i < maxPlatforms; i++)
        {
            Vector2 randomPos = originPosition + new Vector2(Random.Range(horizMin, horizMax), Random.Range(vertMin, vertMax));
            Instantiate(platform, randomPos, Quaternion.identity);
            originPosition = randomPos;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
